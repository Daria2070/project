package com.company;

public class Circle extends Figure {
        private int radius;
public Circle (int centrx,int centry,int radius)
        {
            super(centrx,centry);
            this.radius = radius;
        }
        public int square()
        {
            int a = (int) (Math.PI * Math.pow(this.radius,2));
            return a;
        }
        public int getRadius()
        {
            return this.radius;
        }



}

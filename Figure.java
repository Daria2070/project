package com.company;
public abstract class Figure {
        private int centrx;
        private int centry;
        public Figure (int centrx,int centry)
        {
            this.centrx = centrx;
            this.centry = centry;
        }
        public abstract int square();
        public int getQuadrant(){
        if (centrx>0 && centry>0){
            return 1;
        }
        else if (centrx<0 && centry>0){
            return 2;
        }
        else if (centrx<0 && centry<0){
            return 3;
        }
        else {
            return 4;
        }
    }
        public int getCentrx(){
        return this.centrx;
    }
        public int getCentry(){
        return this.centry;
    }
    }


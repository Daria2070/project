package com.company;

public class Rectangle extends Figure {
        int width;
        int height;
        public Rectangle (int centrx, int centry, int width, int height)
        {
            super(centrx,centry);
            this.width = width;
            this.height = height;
        }

        public int square()
        {
            int a = width * height;
            return a;
        }
        public int getWidth()
        {
            return this.width;
        }
        public int getHeight()
        {
            return this.height;
        }
    }

